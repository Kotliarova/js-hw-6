// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// Екранування потрібно для вірного відображення. Наприклад у нас є рядок 'Моє ім'я - Марина', в такому випадку він закінчується не там де треба - 'Моє ім'. Щоб воно вірно відображалося потрібно додати символ екранування \, тобто 'Моє ім\'я - Марина' і ми отрамємо те що треба. Також гарною ідеєю буде зробити шаблоний рядок тоді таких проблем не виникне, а саме `Моє ім'я - Марина`

// Які засоби оголошення функцій ви знаєте?
// function declaration:
// function name() {};
// function expressions:
// const name = function() {};
// i стрілкова функція:
// let sum = (a, b) => a + b

// Що таке hoisting, як він працює для змінних та функцій?
// Це коли змінні та оголошення функції піднімаються вгору по своїй області видимості перед виконанням коду. Тобто спочатку інтерпритатор пробігається по коду та дивиться на зміні та оголошення функцій і так би мовити бере собі на замітку що вони існують. Це дозволяє викликати функцію чи зміну до її оголошення. Проте не всі змінні чи функції можна викликати до їх оголошення. Щодо зміних то це тільки var (інші const та let можна викликати тільки після їх оголошення) і тільки функції які оголошенні засобом function declaration можна викликати до їх оголошення.

function createNewUser() {
    const userFirstName = prompt('Enter your name');
    const userLastName = prompt('Enter your last name');
    const birthday = (prompt('Enter your birthday', 'dd.mm.yyyy'));
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        birthday,
        getLogin: function() {
            return (newUser.firstName.slice(0,1) + newUser.lastName).toLowerCase();
        },
        getAge: function() {
            const currentDate = new Date();
            const userBirthdayDay = newUser.birthday.split('.')[0];
            const userBirthdayMonth = newUser.birthday.split('.')[1];
            const userBirthdayYears = newUser.birthday.split('.')[2];
            const userBirthday = new Date(userBirthdayYears, userBirthdayMonth - 1, userBirthdayDay);
            const userAge = Math.floor((currentDate - userBirthday) / (1000 * 60 * 60 * 24 * 365));
            return userAge;

        },
        getPassword: function() {
            return newUser.firstName.slice(0,1).toUpperCase() + newUser.lastName.toLowerCase() + newUser.birthday.split('.')[2];
        },
        setfirstName: function(firstName) {
            this.firstName = firstName;
        },
        setlastName: function(lastName) {
            this.lastName = lastName;
        }
    }
    return newUser;
    
}
let user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());
